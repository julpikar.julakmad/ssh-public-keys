# How to load new keys on a server

```
curl https://gitlab.com/HRMWEB/ssh-public-keys/-/raw/main/authorized_keys > ~/.ssh/authorized_keys.new && \
  cp ~/.ssh/authorized_keys{,.old} && \
  cp ~/.ssh/authorized_keys{.new,}
```

After downloading the new authorized_keys, make sure to inspect it to ensure it is correct, and test logging in using a new terminal before closing the existing terminal.
